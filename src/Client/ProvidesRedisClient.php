<?php

namespace Redis\Client;

use Predis\Client as RedisClient;

trait ProvidesRedisClient
{
    /** @var RedisClient */
    protected $redisClient;

    /**
     * @return RedisClient
     */
    public function getRedisClient(): RedisClient
    {
        return $this->redisClient;
    }

    /**
     * @param RedisClient $redisClient
     */
    public function setRedisClient(RedisClient $redisClient)
    {
        $this->redisClient = $redisClient;
    }
}