<?php

namespace Redis\Client\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Predis\Client as RedisClient;

class RedisClientFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');

        $parameters = [];
        if (isset($config['predis']) && is_array($config['predis'])) {
            $parameters = $config['predis'];
        }

        return new RedisClient($parameters);
    }
}