<?php

namespace Redis;

use Predis\Client as RedisClient;
use Redis\Client\Factory\RedisClientFactory;

return [
    'predis' => [
        'scheme' => 'tcp',
        'host' => 'localhost',
        'port' => '6379',
    ],

    'service_manager' => [
        'factories' => [
            RedisClient::class => RedisClientFactory::class
        ]
    ]
];